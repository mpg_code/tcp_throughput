#include "Timeslice.h"

int Timeslice::numTimeslices = 1;

/* Methods for class Timeslice */
Timeslice::Timeslice(struct timeval *tv){
  index = numTimeslices++;
  /* Insert start tv, calculate end tv */
  startTime = *tv;
  timeradd(tv, &(GlobOpts::interval), &endTime);
  if ( GlobOpts::debug )
    cerr << "Created new Timeslice" << endl;
}

void Timeslice::printBoundaries(){
  cerr << "Start tv - sec : " << startTime.tv_sec << endl;
  cerr << "Start tv - usec : " << startTime.tv_usec << endl;
  cerr << "End tv - sec : " << endTime.tv_sec << endl;
  cerr << "End tv - usec : " << endTime.tv_usec << endl;
}

void Timeslice::addStreamSlice(uint32_t iPort, StreamSlice* ss){
  /* Check if port is found in map.
     If found: ignore
     If not found: Add StreamSlice to map. */
  if( streamslices.count(iPort) == 0 ) {
    streamslices[iPort] = ss;
  }
}

void Timeslice::printStats(){ 
  cout << "Timeslice " << index << " - ";
  int aggThr = sumStreamslices();
  cout << "Aggregate Throughput: " << aggThr << endl;
  
}

int Timeslice::sumStreamslices(){
  int sum = 0;
  map<uint32_t, StreamSlice*>::iterator it, itEnd;
  int count = streamslices.size();
  cerr << "Size of map: " << count << endl;
  if ( count > 0 ){
    it = streamslices.begin();
    itEnd = streamslices.end();
    for(; it != itEnd; it++){
      cerr << "streamslice iPort: " << it->first << endl;
      cerr << "Throughput: " << it->second->getThroughput() << endl;
      sum += it->second->getThroughput();
    }
  }else{
    cerr << "Empty timeslice" << endl;
  }
  return sum;
}

// void Timeslice::fillGaps(){
  
//   /* Traverse all streams. Create empty StreamSlices for each
//      non-represented stream */
//   StreamSlice *ss = new StreamSlice(this);
//   static map<int, Stream*>::iterator it, itEnd;
//   it = GlobOpts::streams.begin();
//   itEnd = GlobOpts::streams.end();
//   for(; it != itEnd; it++){
//     uint16_t portNumber = it->first;
//     if ( streamslices.count(portNumber) == 0 ){
//       streamslices[portNumber] = ss;
//     }
//   }
  
// }

int Timeslice::getTpByPort(uint32_t p){
  if (streamslices.count(p) > 0){
    return streamslices[p]->getThroughput();
  }else{
    return 0;
  }
}
