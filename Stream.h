#ifndef STREAM_H
#define STREAM_H

#include "tcp-throughput.h"
#include "StreamSlice.h"

/* Forward declarations */
class StreamSlice;
class Timeslice;

using namespace std;

/* Represents one connection (srcport/dstport pair) */
class Stream {

 private:
  uint16_t srcPort;
  uint32_t iPort; /* Concatenated IP and Port number */
  //uint16_t dstPort;
  uint32_t largestAck;
  int throughput;
  vector<StreamSlice*> streamslices;
  
 public:
  Stream(uint32_t ipt, uint16_t src);

  ~Stream(){}

  uint16_t getSrcPort() { return srcPort; }
  uint32_t getIPort() { return iPort; }
  StreamSlice* registerAck(uint32_t ack, Timeslice* ts);
  void printStats();
  
};
#endif /* STREAM_H */
