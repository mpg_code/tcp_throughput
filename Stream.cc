#include "Stream.h"

/* Methods for class Stream */
Stream::Stream(uint32_t ipt, uint16_t src){
  largestAck = 0;
  throughput = 0;
  iPort = ipt;
  srcPort = src;
}

StreamSlice* Stream::registerAck(uint32_t ack, Timeslice* ts){
  StreamSlice* tmpStSl;
  if ( GlobOpts::debug )
    cerr << "ACK: " << ack << endl;
  
  if ( !(streamslices.empty()) ){
    tmpStSl = streamslices.back();
    
    /* If the "current" timslice is different from the last registered*/    
    if ( ts != tmpStSl->getTs() ) {
      /* Create a new StreamSlice */
      tmpStSl = new StreamSlice(ts);
      streamslices.push_back(tmpStSl);
      if ( GlobOpts::debug )
	cerr << "created new StreamSlice for stream " << iPort << endl;
    }
    /* Add data to aggregated (stream) thoughput */
    if ( GlobOpts::debug ){
      cerr << "ack       : " << ack << endl;
      cerr << "largestAck: " << largestAck << endl;
    }
    if ( largestAck > ack ) {
      cerr << "Port reused or ack number wrapped - skipping StreamSlice" << endl;
      largestAck = ack;
      return tmpStSl;
    }
    uint32_t newPut = ack - largestAck;
    if ( newPut > MAX_WINDOW_SIZE ){
      cerr << "Acked bytes: " << newPut << endl;
      cerr << "ACK size too large - possible reused port - skipping StreamSlice" << endl;
      largestAck = ack;
      return tmpStSl;
    }
    throughput += newPut;
    /* Add data to StreamSlice throughput */
    tmpStSl->updateThroughput(newPut);
    if ( GlobOpts::debug )
      cerr << "added " << newPut << " bytes to throughput" << endl;
  } else {
    /* Create first StreamSlice */
    tmpStSl = new StreamSlice(ts);
    streamslices.push_back(tmpStSl);
    if ( GlobOpts::debug )
      cerr << "creating first StreamSlice for stream " << iPort << endl;
  }

  largestAck = ack;
  if ( GlobOpts::debug ){
    cerr << "End of registerAck" << endl;
    cerr << "StreamSlice throughput: " << tmpStSl->getThroughput() << endl;
  }
  return tmpStSl;
}

void Stream::printStats(){
  /* Traverse Timeslices 
     for each timeslice, calculate output (if any) if no data for the
     current port number, print a zero */
  vector<Timeslice*>::iterator it, itEnd;
  it = GlobOpts::timeslices.begin();
  itEnd = GlobOpts::timeslices.end();

  // /* Output stream number first */
  // cout << iPort << ", ";
  for(; it != itEnd; it++){
    if ( it == itEnd - 1)
      cout << (*it)->getTpByPort(iPort) << endl;
    else
      cout << (*it)->getTpByPort(iPort) << ", ";
  }
}
