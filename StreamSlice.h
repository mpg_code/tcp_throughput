#ifndef STREAMSLICE_H
#define STREAMSLICE_H

#include <stdint.h>
#include <iostream>
//#include "Timeslice.h"

using namespace std;

/* Forward declarations */
class Timeslice;

class StreamSlice {

 private:
  Timeslice *ts;
  int throughput;

 public:
  StreamSlice(Timeslice* curTs);

  ~StreamSlice(){}
  Timeslice* getTs() { return ts; }
  void updateThroughput(int val) { throughput += val; }
  int getThroughput() { return throughput; }
};
#endif /* STREAMSLICE_H */
