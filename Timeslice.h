#ifndef TIMESLICE_H
#define TIMESLICE_H

#include <map>
#include <sys/time.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tcp-throughput.h"
#include "StreamSlice.h"

using namespace std;

/* Forward declarations */
class Stream;

/* Represents one connection (srcport/dstport pair) */
class Timeslice {

 private:
  struct timeval startTime;
  struct timeval endTime;
  static int numTimeslices;
  int index;
  map<uint32_t, StreamSlice*> streamslices;
  int sumStreamslices();

  
 public:
  Timeslice(struct timeval *tv);

  ~Timeslice(){}

  struct timeval *getEndTime() { return &endTime; }
  struct timeval *getStartTime() { return &startTime; }
  void addStreamSlice(uint32_t iPort, StreamSlice* ss);
  void printBoundaries();
  void printStats();
  void fillGaps();
  int getTpByPort(uint32_t p);
  int getIndex() { return index; }

};
#endif /* TIMESLICE_H */
