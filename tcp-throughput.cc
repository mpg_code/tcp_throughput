#include "tcp-throughput.h"

/* Global vector of timeslices for analysis */
vector<Timeslice*> GlobOpts::timeslices;
map<uint32_t, Stream*> GlobOpts::streams;
struct timeval GlobOpts::interval;
bool GlobOpts::debug;

void usage (char* argv){
  printf("Usage: %s [-s] [-r] [-p] [-f]\n", argv);
  printf("Required options:\n");
  printf(" -s <srcIP1,srcIP2,...>  : Sender ips.\n");
  printf(" -r <rcvIP1,rcvIP2,...>  : Receiver ip.\n");
  printf(" -p <receiver port>      : Receiver port.\n");
  printf(" -f <pcap-file>          : Sender-side dumpfile.\n");
  printf("Other options:\n");
  printf(" -t                      : Timeslot size (default 100ms)\n");
  printf(" -d                      : Print extensive debug information\n");
  exit(0);
}

/* pcap processing method */
void processAck(const struct pcap_pkthdr* header, const u_char *data){
  const struct sniff_ethernet *ethernet; /* The ethernet header */
  const struct sniff_ip *ip; /* The IP header */
  const struct sniff_tcp *tcp; /* The TCP header */
  //const char *payload; /* Packet payload */
  const struct timeval *hdrTv; /* Pcap timestamp */
  Timeslice *curTs;

  /* Finds the different headers+payload */
  ethernet = (struct sniff_ethernet*)(data);
  ip = (struct sniff_ip*)(data + SIZE_ETHERNET);
  //u_int ipSize = ntohs(ip->ip_len);
  u_int ipHdrLen = IP_HL(ip)*4;
  tcp = (struct sniff_tcp*)(data + SIZE_ETHERNET + ipHdrLen);
  //u_int tcpHdrLen = TH_OFF(tcp)*4;
  hdrTv = &(header->ts);
  uint16_t port = ntohs(tcp->th_dport);
  
  char iPortStr[20];
  uint32_t iPort;
  sprintf(iPortStr,"%u%u", (uint32_t)(ip->ip_src.s_addr), port);
  iPort = atoi(iPortStr);
  if (GlobOpts::debug)
    cerr << "iPort: " << iPort << endl;
  
  if( GlobOpts::debug)
    cerr << "port: " << port << endl;
  /* If no first time has been set, record first time 
     and create first timeslice */
  if( GlobOpts::timeslices.empty() ){
    curTs = new Timeslice((timeval*)hdrTv);
    GlobOpts::timeslices.push_back(curTs);
  }

  /* Add new timeslices until the timestamp is within
     the limits of the "current" timeslice */
  bool done = false;
  while ( !done ){
    //struct timeval *startTime = GlobOpts::timeslices.back()->getStartTime();
    struct timeval *endTime = GlobOpts::timeslices.back()->getEndTime();
    
    /* If this packet's timestamp is larger than
       the current timeslice: add new timeslice */
    if ( timercmp(hdrTv, endTime, > ) ){
      curTs = new Timeslice(endTime);
      GlobOpts::timeslices.push_back(curTs);
    }else{ /* Since packets are in order, and
	      this packet's timestamp is not larger 
	      than the end timestamp of the timeslice,
	      the packet is within the limits of the 
	      current timeslice */
      curTs = GlobOpts::timeslices.back();
      done = true;
    }
  }

  Stream* curSt;
  /* Find correct stream */
  if ( GlobOpts::streams.count(iPort) > 0) {
    map<uint32_t, Stream*>::iterator cur  = GlobOpts::streams.find(iPort);
    if( GlobOpts::debug )
      cerr << "Found stream with iPort number " << iPort << endl; 
    curSt = cur->second;
  } else {
    curSt = new Stream(iPort, port);
    GlobOpts::streams[iPort] = curSt;
    
    if( GlobOpts::debug)
      cerr << "Created new stream with iPort number " << iPort << endl;
  }
  
  /* Register ack data */
  StreamSlice* tmpStSl = curSt->registerAck(ntohl(tcp->th_ack), curTs);
  
  /* Add new StreamSlice to Timeslice */
  curTs->addStreamSlice(iPort, tmpStSl);
}

int main(int argc, char *argv[]){
  char errbuf[PCAP_ERRBUF_SIZE];
  char *src_ip = (char*)"192.168.1.1";
  char *dst_ip = (char*)"1.0.5.1";
  int dst_port = 12345;
  //int timeslot;
  string filename; /* Sender dump file name */
  uint32_t val;
  struct timeval iv; /* Timeslice duration */
  iv.tv_sec = 0;
  iv.tv_usec = 0;
   
  int c;

  while(1){
    c = getopt( argc, argv, "s:r:p:f:t:d");
    if(c == -1) break;

    switch(c){
    case 's':
      src_ip = optarg;
      break;
    case 'r':
      dst_ip = optarg;
      break;
    case 'p':
      dst_port = atoi(optarg);
      break;
    case 'f':
      filename = optarg;
      break;
    case 'd':
      GlobOpts::debug = true;
      break;
    case 't':
      val = atoi(optarg) * 1000;
      while ( val >= 1000000 ){
	val = val - 1000000;
	iv.tv_sec++;
      }
      iv.tv_usec = val;
      break;
    case '?':
      if(isprint(optopt))
	fprintf(stderr,"Unknown option -%c\n", optopt);
      else
	fprintf(stderr, "Something is really wrong\n"); 
      return 1;
    default:
      break;
    }
  }

  if(argc < 5){
    usage(argv[0]);
  }

  if ( iv.tv_sec == 0 && iv.tv_usec == 0 ) {
    iv.tv_usec = 100000;
  }

  /* Set timeslice duration */
  GlobOpts::interval = iv;
  
  if( GlobOpts::debug){
    cerr << "Interval timeval:" << endl;
    cerr << "sec : " << iv.tv_sec << endl;
    cerr << "usec : " << iv.tv_usec << endl;
  }

  struct pcap_pkthdr h;
  const u_char *data;
  
  pcap_t *fd = pcap_open_offline(filename.c_str(), errbuf);
  if ( fd == NULL ) {
    cerr << "pcap: Could not open file" << filename << endl;
    exit(1);
  }

  /* Set up pcap filter to include only incoming tcp
     packets with the ACK flag set and correct ip and 
     port numbers. */
  struct bpf_program compFilter;
  stringstream filterExp;
  
  /* Parse ip addresses */
  filterExp << "tcp && (";
  char *hip = NULL;
  const char *delim = ",";
  hip = strtok(dst_ip, delim);
  while( hip != NULL ) {
    filterExp << "src host " << hip;
    hip = strtok( NULL, delim );
    if ( hip )
      filterExp << " || ";
  }
  filterExp << ") && (";
  
  hip = NULL;
  hip = strtok(src_ip, delim);
  while( hip != NULL ) {
    filterExp << "dst host " << hip;
    hip = strtok( NULL, delim );
    if ( hip )
      filterExp << " || ";
  }
  filterExp << ") && src port " << dst_port 
	    << " && tcp[13] & 16 = 16";

  if( GlobOpts::debug)
    cerr << filterExp.str() << endl;
  
  /* Apply filter */
  if (pcap_compile(fd, &compFilter, (char*)((filterExp.str()).c_str()), 0, 0) == -1) {
    cerr << "Couldn't parse filter " << filterExp << "Error:" << pcap_geterr(fd) << endl;
    exit(1);
  }
  
  if (pcap_setfilter(fd, &compFilter) == -1) {
    cerr << "Couldn't install filter: " << filterExp << "Error: " << pcap_geterr(fd) << endl;
    exit(1);
  }
  
  /* Sniff each ACK packet in pcap tracefile: */
  do {
    data = (const u_char *)pcap_next(fd, &h);
    if(data == NULL){
      char errMsg[] = "\nNo more data on file\n";
      pcap_perror(fd, errMsg);
    }else{
      processAck(&h, data); /* Sniff packet */
    }
  } while(data != NULL);
  
  pcap_close(fd);
  
  /*******************************************
   ** Finished building the data structure. **
   ** begin producing statistical data.     **
   ******************************************/

  // /* First print index label */
  // cout << "port, ";
  
  /* As of now the output is a matrix of timeslices and port numbers
     giving the throughput of each stream per timeslice
     this is done to be able to process the statistics using the 
     statistical language/tool R 
     Format:

     port    1   2   3   4   5 ...
     15001   tp  tp  tp  tp  tp
     15002   tp  tp  tp  tp  tp
     ...

     Traverse to print timeslice number */
  vector<Timeslice*>::iterator vIt, vItEnd;
  vIt = GlobOpts::timeslices.begin();
  vItEnd = GlobOpts::timeslices.end();
  for(; vIt != vItEnd; vIt++){
    if( vIt != vItEnd-1 ){
      cout << (*vIt)->getIndex() << ", ";
    }else{
      cout << (*vIt)->getIndex() << endl;
    }
  }

  map<uint32_t, Stream*>::iterator it, itEnd;
  it = GlobOpts::streams.begin();
  itEnd = GlobOpts::streams.end();
  for(; it != itEnd; it++){
    it->second->printStats();
  }  
  /* Generate statistics */
  /* 1) Traverse timeslices and print number of registered streams and 
     aggregated throughput for each timeslice */
  
//   vector<Timeslice*>::iterator it, itEnd;
//   it = GlobOpts::timeslices.begin();
//   itEnd = GlobOpts::timeslices.end();
//   for(; it != itEnd; it++){
//     (*it)->printStats();
//   }  
   
  return 0;
}

